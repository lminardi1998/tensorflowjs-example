import * as tf from '@tensorflow/tfjs';
import {load_csv, print_dataset_head} from './src/data/data';
import {preprocess_dataset} from './src/pipelines/processing';
import { build_regression_model } from './src/model_factory/regression';


async function main(){
    var df = load_csv("file://C:/Users/lminardi.inst/projects/Tesi/tesi_code/client/data/raw/medical_cost.csv")
    const flattenedDataset = preprocess_dataset(df)
    // Define the model.
    const model = build_regression_model([2]);
    
    model.compile({
      optimizer: tf.train.adam(),
      loss: 'meanAbsoluteError'
    });
    
    // Fit the model using the prepared Dataset
    const history =  await model.fitDataset(flattenedDataset, {
      epochs: 10,
      callbacks: {
        onEpochEnd: async (epoch, logs) => {
          console.log(logs);
          console.log(epoch + ':' + logs.loss);
        }
      }
    });
}

main()
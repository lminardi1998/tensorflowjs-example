import * as tf from '@tensorflow/tfjs';

function build_regression_model(input_shape:Array<number>):tf.LayersModel{
    const input = tf.input({shape: input_shape});
    const denseLayer1 = tf.layers.dense({units: 10, activation: 'relu'});
    const denseLayer2 = tf.layers.dense({units: 1, activation: 'linear'});
    // Obtain the output symbolic tensor by applying the layers on the input.
    const output = denseLayer2.apply(denseLayer1.apply(input));
    return tf.model({inputs: input, outputs: (output as tf.SymbolicTensor)});
}

export {build_regression_model}
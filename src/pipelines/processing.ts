
import * as tf from '@tensorflow/tfjs';

function preprocess_dataset(df:tf.data.CSVDataset): tf.data.CSVDataset{
    const  flattenedDataset =  df.map((e) =>
        {
        // Convert xs(features) and ys(labels) from object form (keyed by
        // column name) to array form.
        return {xs:[e['xs']["age"],e["xs"]["bmi"]], ys:Object.values(e['ys'])};
        }).batch(10);
    return (flattenedDataset as tf.data.CSVDataset)
}
export {preprocess_dataset }
import * as tf from '@tensorflow/tfjs';

function load_csv(csv_path:string): tf.data.CSVDataset{
    const csvDataset = tf.data.csv(
        csv_path, {
            columnConfigs: {
                charges: {
                  isLabel: true
                }
              }
        });
    return csvDataset
}
 
async function print_dataset_head(dataset:tf.data.CSVDataset, iter:number = 5):Promise<void>{
    // let iterator = await dataset.iterator().then((it) => {
    //     for (let i = 0; i < iter; i++) {
    //         const e = it.next().then((e) => console.log(e.value))
    //     }
    // })
    await dataset.take(iter).forEachAsync((e) => console.log(e))
}





export {load_csv, print_dataset_head }